# Gifty
Gifty is a completely free and open source multiplatform gift exchange tool
modeled after the age-old tradition of drawing names from a bowl. Enter the
names of the participants and pass the device around while everyone is randomly
assigned to each other.

## Compilation
Those seeking to compile must have...

- Node.js 16.2

Once these requirements have been met, simply clone the repository and refer to
the official Ionic documentation for further instructions on how to build.
